/*
 * @Autor: cjh
 * @Date: 2022-03-20 21:24:33
 * @LastEditors: cjh
 * @LastEditTime: 2022-04-23 17:03:10
 * @Description:
 */
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

createApp(App).use(store).use(router).use(Antd).mount('#app')
