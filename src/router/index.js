/*
 * @Author: cjh
 * @Date: 2022-03-20 21:24:33
 * @LastEditors: cjh
 * @LastEditTime: 2022-04-23 17:37:12
 * @Description:
 */
import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '../layout'
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Layout,
    children: [
      {
        path: '/home',
        name: 'Home',
        component: () => import('../views/Home.vue'),
      },
      {
        path: '/api',
        name: 'Api',
        component: () => import('../views/Api.vue'),
      },
      {
        path: '/demo',
        name: 'Demo',
        component: () => import('../views/Demo.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
